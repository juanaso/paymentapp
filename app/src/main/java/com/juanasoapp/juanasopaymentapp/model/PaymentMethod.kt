package com.juanasoapp.juanasopaymentapp.model

data class PaymentMethod (

	val id : String,
	val name : String,
	val payment_type_id : String,
	val status : String,
	val secure_thumbnail : String,
	val thumbnail : String,
	val deferred_capture : String,
	val settings : List<Settings>,
	val additional_info_needed : List<String>,
	val min_allowed_amount : String,
	val max_allowed_amount : String,
	val accreditation_time : String,
	val financial_institutions : List<String>,
	val processing_modes : List<String>
)
data class Settings (

	val card_number : CardNumber,
	val bin : Bin,
	val security_code : SecurityCode
)

data class CardNumber (

	val validation : String,
	val length : String
)

data class Bin (

	val pattern : String,
	val installments_pattern : String,
	val exclusion_pattern : String
)

data class SecurityCode (

	val length : String,
	val card_location : String,
	val mode : String
)
