package com.juanasoapp.juanasopaymentapp.model


data class Installment (

	val payment_method_id : String,
	val payment_type_id : String,
	val issuer : IssuerInstallment,
	val processing_mode : String,
	val merchant_account_id : String,
	val payer_costs : List<PayerCosts>,
	val agreements : String
)

data class IssuerInstallment (

	val id : String,
	val name : String,
	val secure_thumbnail : String,
	val thumbnail : String
)

data class PayerCosts (

	val installments : String,
	val installment_rate : String,
	val discount_rate : String,
	val reimbursement_rate : String,
	val labels : List<String>,
	val installment_rate_collector : List<String>,
	val min_allowed_amount : String,
	val max_allowed_amount : String,
	val recommended_message : String,
	val installment_amount : String,
	val total_amount : String,
	val payment_method_option_id : String
)