package com.juanasoapp.juanasopaymentapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.juanasoapp.juanasopaymentapp.PaymentApplication

class PaymentApplicationViewModelFactory  (private val paymentApplication: PaymentApplication):
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        return modelClass.getConstructor(PaymentApplication::class.java)
            .newInstance(paymentApplication)
    }
}