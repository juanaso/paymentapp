package com.juanasoapp.juanasopaymentapp.viewmodel

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.juanasoapp.juanasopaymentapp.PaymentApplication
import com.juanasoapp.juanasopaymentapp.network.NetworkApplication

abstract class StateViewModel(app: PaymentApplication, val state: SavedStateHandle) : ViewModel() {class Factory(val app: NetworkApplication, private val owner: SavedStateRegistryOwner, args: Bundle?) : AbstractSavedStateViewModelFactory(owner, args) {override fun <T : ViewModel?> create(key: String, modelClass: Class<T>, handle: SavedStateHandle): T {return modelClass.getConstructor(PaymentApplication::class.java, SavedStateHandle::class.java).newInstance(app, handle)}}}