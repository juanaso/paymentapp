package com.juanasoapp.juanasopaymentapp.payment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders

import com.juanasoapp.juanasopaymentapp.R
import kotlinx.android.synthetic.main.fragment_success.*

class SuccessFragment : Fragment() {
    lateinit var viewModel: PaymentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_success, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = activity.let { ViewModelProviders.of(it!!).get(PaymentViewModel::class.java) }
        banco.text ="Banco :" + viewModel.currentIssuer.name
        amount.text = viewModel.currentInstallment.recommended_message

        when(viewModel.currentPaymentMethod.payment_type_id){
            "credit_card" -> payment.text = "Pago con Tarjeta de Credito"
            "debit_card" -> payment.text = "Pago con Tarjeta de Debito"
            "atm" -> payment.text = "Pago con un ATM"
            "ticket" -> payment.text = "Pago con Ticket"
        }

        continueButton.isEnabled = true
        continueButton.setOnClickListener {
            viewModel.goToFirst()
        }
    }

    companion object {
        fun getFragmentName(): String {
            return SuccessFragment::class.java.name
        }
    }

}
