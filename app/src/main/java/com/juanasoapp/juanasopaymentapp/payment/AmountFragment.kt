package com.juanasoapp.juanasopaymentapp.payment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.juanasoapp.juanasopaymentapp.R
import kotlinx.android.synthetic.main.fragment_amount.*

class AmountFragment : Fragment() {

    lateinit var viewModel: PaymentViewModel
    var currentAmount = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_amount, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = activity.let { ViewModelProviders.of(it!!).get(PaymentViewModel::class.java) }
        continueButton.setOnClickListener { viewModel.setAmount(currentAmount) }

        amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                amount.removeTextChangedListener(this)
                val stringText = s.toString()
                val cleanString = stringText.replace("$ ", "")
                if (cleanString.isNotEmpty() || cleanString.isNotBlank()) {
                    currentAmount = cleanString
                    val toShow = "$ $cleanString"
                    amount.setText(toShow)
                    amount.setSelection(toShow.length)
                } else {
                    amount.setText("")
                }
                amount.addTextChangedListener(this)
                validButton()
            }
        })
        amount.setText(viewModel.currentAmount)
    }

    private fun validButton() {
        continueButton.isEnabled = amount.text.isNotEmpty() && amount.text.isNotBlank()
    }

    companion object {
        fun getFragmentName(): String {
            return AmountFragment::class.java.name
        }
    }
}
