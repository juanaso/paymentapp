package com.juanasoapp.juanasopaymentapp.payment.installments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.juanasoapp.juanasopaymentapp.GenericAdapter

import com.juanasoapp.juanasopaymentapp.R
import com.juanasoapp.juanasopaymentapp.model.PayerCosts
import com.juanasoapp.juanasopaymentapp.payment.PaymentViewModel
import kotlinx.android.synthetic.main.fragment_select_installments.*


class SelectInstallmentsFragment : Fragment() {
    lateinit var viewModel: PaymentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_installments, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = activity.let { ViewModelProviders.of(it!!).get(PaymentViewModel::class.java) }


        val myAdapter = object : GenericAdapter<Any>(ArrayList<PayerCosts>()) {
            override fun getLayoutId(position: Int, obj: Any): Int {
                return R.layout.installments_row
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                val viewHolder = InstallmentViewHolder(view)
                viewHolder.onItemCLick = {
                    viewModel.setInstallment(it)
                }
                return viewHolder
            }
        }

        insallmentsRecycler.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        insallmentsRecycler.adapter = myAdapter

        viewModel.installments.observe(viewLifecycleOwner, Observer {
            cl_lottie_container.isVisible = false
            if(!it.isNullOrEmpty()){
                recyclerContainer.isVisible = true
                myAdapter.setItems(it[0].payer_costs)
            }else{
                emptyMessage.isVisible = true
            }

        })

        viewModel.getInstallments()
    }

    companion object {
        fun getFragmentName(): String {
            return SelectInstallmentsFragment::class.java.name
        }
    }

}
