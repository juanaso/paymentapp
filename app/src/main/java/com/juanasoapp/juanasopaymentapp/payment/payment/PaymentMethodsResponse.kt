package com.juanasoapp.juanasopaymentapp.payment.payment

import com.juanasoapp.juanasopaymentapp.model.PaymentMethod

class PaymentMethodsResponse: ArrayList<PaymentMethod>()