package com.juanasoapp.juanasopaymentapp.payment.payment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.juanasoapp.juanasopaymentapp.GenericAdapter
import com.juanasoapp.juanasopaymentapp.R
import com.juanasoapp.juanasopaymentapp.model.PaymentMethod
import com.juanasoapp.juanasopaymentapp.payment.PaymentViewModel
import kotlinx.android.synthetic.main.fragment_payments.*


class SelectPaymentMethodFragment : Fragment() {

    lateinit var viewModel: PaymentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payments, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = activity.let { ViewModelProviders.of(it!!).get(PaymentViewModel::class.java) }

        val myAdapter = object : GenericAdapter<Any>(ArrayList<PaymentMethod>()) {
            override fun getLayoutId(position: Int, obj: Any): Int {
                return R.layout.payment_method_row
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                val viewHolder = PaymentMethodViewHolder(view)
                viewHolder.onItemCLick = {
                    viewModel.setPaymentMethod(it)
                }
                return viewHolder
            }
        }

        paymentRecycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        paymentRecycler.adapter = myAdapter
        viewModel.paymentMethods.observe(viewLifecycleOwner, Observer {
            cl_lottie_container.isVisible = false
            paymentRecycler.isVisible = true
            myAdapter.setItems(it)
        })
        viewModel.getPaymentMethods()

    }

    companion object {
        fun getFragmentName(): String {
            return SelectPaymentMethodFragment::class.java.name
        }
    }
}
