package com.juanasoapp.juanasopaymentapp.payment

import com.juanasoapp.juanasopaymentapp.PaymentApplication
import com.juanasoapp.juanasopaymentapp.network.Callback
import com.juanasoapp.juanasopaymentapp.network.ServiceGenerator
import com.juanasoapp.juanasopaymentapp.payment.installments.InstallmentsResponse
import com.juanasoapp.juanasopaymentapp.payment.issuer.CardIssuersResponse
import com.juanasoapp.juanasopaymentapp.payment.payment.PaymentMethodsResponse

class MercadoPagoRepository(private val application: PaymentApplication){
    var onSuccessIdCall: ((PaymentMethodsResponse) -> Unit)? = null
    var onSuccessIsuersCall: ((CardIssuersResponse) -> Unit)? = null
    var onSuccessInstallmentsCall: ((InstallmentsResponse) -> Unit)? = null


    fun getPaymentMethods(publicKey:String) {
        ServiceGenerator.createService(application, PaymentServices::class.java)
            .getMethods(publicKey)
            .enqueue(Callback<PaymentMethodsResponse>(application).apply {
                this.onSuccess = {
                    onSuccessIdCall?.invoke(it!!)}
            })
    }

    fun getIssuers(publicKey:String,paymentMethodId: String) {
        ServiceGenerator.createService(application, PaymentServices::class.java)
            .getIssuers(publicKey,paymentMethodId)
            .enqueue(Callback<CardIssuersResponse>(application).apply {
                this.onSuccess = {
                    onSuccessIsuersCall?.invoke(it!!)}
            })
    }

    fun getInstallments(publicKey:String,amount: String,paymentMethodId: String,issuerId: String) {
        ServiceGenerator.createService(application, PaymentServices::class.java)
            .getInstallments(publicKey,amount,paymentMethodId,issuerId)
            .enqueue(Callback<InstallmentsResponse>(application).apply {
                this.onSuccess = {
                    onSuccessInstallmentsCall?.invoke(it!!)}
            })
    }
}