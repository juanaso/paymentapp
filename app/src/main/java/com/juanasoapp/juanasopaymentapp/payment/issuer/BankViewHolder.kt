package com.juanasoapp.juanasopaymentapp.payment.issuer

import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.juanasoapp.juanasopaymentapp.GenericAdapter
import com.juanasoapp.juanasopaymentapp.model.Issuer
import kotlinx.android.synthetic.main.bank_row.view.*

class BankViewHolder (view: View) : RecyclerView.ViewHolder(view),
    GenericAdapter.Binder<Issuer> {

    var bankName: TextView = view.issuerName
    var container: ConstraintLayout = view.container
    var onItemCLick: ((Issuer) -> Unit)? = null

    override fun bind(data: Issuer, isLast: Boolean) {
        bankName.text = data.name
        container.setOnClickListener {
            onItemCLick?.invoke(data)
        }
    }
}