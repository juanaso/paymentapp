package com.juanasoapp.juanasopaymentapp.payment

import com.juanasoapp.juanasopaymentapp.payment.installments.InstallmentsResponse
import com.juanasoapp.juanasopaymentapp.payment.issuer.CardIssuersResponse
import com.juanasoapp.juanasopaymentapp.payment.payment.PaymentMethodsResponse
import retrofit2.Call
import retrofit2.http.*

internal interface PaymentServices {

    @GET( "payment_methods")
    fun getMethods(@Query("public_key") publicKey: String): Call<PaymentMethodsResponse>

    @GET( "payment_methods/card_issuers")
    fun getIssuers(@Query("public_key") publicKey: String, @Query("payment_method_id") paymentMethodId: String): Call<CardIssuersResponse>

    @GET( "payment_methods/installments")
    fun getInstallments(@Query("public_key") publicKey: String,
                        @Query("amount") amount: String,
                        @Query("payment_method_id") paymentMethodId: String,
                        @Query("issuer.id") issuerId: String
    ): Call<InstallmentsResponse>
}