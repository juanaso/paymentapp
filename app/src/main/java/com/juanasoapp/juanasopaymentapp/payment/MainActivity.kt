package com.juanasoapp.juanasopaymentapp.payment

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.juanasoapp.juanasopaymentapp.PaymentApplication
import com.juanasoapp.juanasopaymentapp.viewmodel.PaymentApplicationViewModelFactory
import com.juanasoapp.juanasopaymentapp.wizard.BaseWizardActivity

class MainActivity : BaseWizardActivity() {

    lateinit var viewModel: PaymentViewModel

    var currentWizardIndex = 0


    override fun onCreate(savedInstanceState: Bundle?) {

        val ysyEcoViewModelFactory =
            PaymentApplicationViewModelFactory(application as PaymentApplication)
        viewModel = ViewModelProviders.of(this, ysyEcoViewModelFactory)
            .get(PaymentViewModel::class.java)
        super.onCreate(savedInstanceState)

        viewModel.onWizardNext = {
            onNextWizardStep()
        }

        viewModel.onWizardPrevious = {
            onPreviousWizardStep()
        }

        viewModel.goToFirstStep = {
            currentWizardIndex = 1
            onPreviousWizardStep()
        }
    }

    override fun onFinishWizard() {
        super.onFinishWizard()
    }

    override fun getWizardTotalStep(): Int {
        return viewModel.steps.size
    }

    override fun getNextFragmentName(currentFragmentName: String): String {
        currentWizardIndex += 1
        return viewModel.steps[currentWizardIndex]
    }

    override fun getPreviousFragmentName(currentFragmentName: String): String {
        currentWizardIndex -= 1
        return viewModel.steps[currentWizardIndex]
    }

    override fun getStartFragmentName(): String {
        return viewModel.steps[0]
    }

    override fun instantiateFragment(nameFragment: String): Fragment {
        return Fragment.instantiate(this, nameFragment)
    }

    override fun hasFragmentBackEnabled(fragmentName: String): Boolean {
        return true
    }

    override fun onBackPressed() {
        if (currentWizardIndex < 1) {
            finish()
        } else{
            onPreviousWizardStep()
        }
    }
}
