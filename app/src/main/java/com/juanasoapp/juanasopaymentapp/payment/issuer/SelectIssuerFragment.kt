package com.juanasoapp.juanasopaymentapp.payment.issuer

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.juanasoapp.juanasopaymentapp.GenericAdapter

import com.juanasoapp.juanasopaymentapp.R
import com.juanasoapp.juanasopaymentapp.model.Issuer
import com.juanasoapp.juanasopaymentapp.payment.PaymentViewModel
import kotlinx.android.synthetic.main.fragment_select_bank.*


class SelectIssuerFragment : Fragment() {
    lateinit var viewModel: PaymentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_bank, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = activity.let { ViewModelProviders.of(it!!).get(PaymentViewModel::class.java) }


        val myAdapter = object : GenericAdapter<Any>(ArrayList<Issuer>()) {
            override fun getLayoutId(position: Int, obj: Any): Int {
                return R.layout.bank_row
            }

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                val viewHolder =
                    BankViewHolder(view)
                viewHolder.onItemCLick = {
                    viewModel.setIssuer(it)
                }
                return viewHolder
            }
        }

        issuerRecycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        issuerRecycler.adapter = myAdapter
        viewModel.issuers.observe(viewLifecycleOwner, Observer {
            cl_lottie_container.isVisible = false

            if (!it.isNullOrEmpty()){
                myAdapter.setItems(it)
                issuerRecycler.isVisible = true
                emptyMessage.isVisible = false
            }else{
                emptyMessage.isVisible = true
            }

        })
        viewModel.getIssuers()
    }

    companion object {
        fun getFragmentName(): String {
            return SelectIssuerFragment::class.java.name
        }
    }
}
