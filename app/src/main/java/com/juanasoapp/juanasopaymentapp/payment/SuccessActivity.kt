package com.juanasoapp.juanasopaymentapp.payment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.juanasoapp.juanasopaymentapp.R

class SuccessActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success)
    }
}
