package com.juanasoapp.juanasopaymentapp.payment.payment

import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.juanasoapp.juanasopaymentapp.GenericAdapter
import com.juanasoapp.juanasopaymentapp.model.PaymentMethod
import kotlinx.android.synthetic.main.payment_method_row.view.*

class PaymentMethodViewHolder (view: View) : RecyclerView.ViewHolder(view),
    GenericAdapter.Binder<PaymentMethod> {

    var bankName: TextView = view.payment
    var container: ConstraintLayout = view.container
    var onItemCLick: ((PaymentMethod) -> Unit)? = null

    override fun bind(data: PaymentMethod, isLast: Boolean) {
        bankName.text = data.name

        container.setOnClickListener {
            onItemCLick?.invoke(data)
        }
    }
}