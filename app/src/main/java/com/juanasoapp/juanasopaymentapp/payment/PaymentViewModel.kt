package com.juanasoapp.juanasopaymentapp.payment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.juanasoapp.juanasopaymentapp.Constants.publicKey
import com.juanasoapp.juanasopaymentapp.PaymentApplication
import com.juanasoapp.juanasopaymentapp.model.Issuer
import com.juanasoapp.juanasopaymentapp.model.PayerCosts
import com.juanasoapp.juanasopaymentapp.model.PaymentMethod
import com.juanasoapp.juanasopaymentapp.payment.installments.SelectInstallmentsFragment
import com.juanasoapp.juanasopaymentapp.payment.installments.InstallmentsResponse
import com.juanasoapp.juanasopaymentapp.payment.issuer.CardIssuersResponse
import com.juanasoapp.juanasopaymentapp.payment.issuer.SelectIssuerFragment
import com.juanasoapp.juanasopaymentapp.payment.payment.PaymentMethodsResponse
import com.juanasoapp.juanasopaymentapp.payment.payment.SelectPaymentMethodFragment


class PaymentViewModel(private val app: PaymentApplication) : ViewModel() {
    var steps: List<String> =
        listOf(AmountFragment.getFragmentName(),
            SelectPaymentMethodFragment.getFragmentName(),
            SelectIssuerFragment.getFragmentName(),
            SelectInstallmentsFragment.getFragmentName(),
            SuccessFragment.getFragmentName())

    val repository = MercadoPagoRepository(app)
    val paymentMethods = MutableLiveData<PaymentMethodsResponse>()
    val issuers = MutableLiveData<CardIssuersResponse>()
    val installments = MutableLiveData<InstallmentsResponse>()
    var onWizardNext: (() -> Unit)? = null
    var onWizardPrevious: (() -> Unit)? = null
    var goToFirstStep: (() -> Unit)? = null
    var currentAmount = ""
    lateinit var currentPaymentMethod : PaymentMethod
    lateinit var currentIssuer : Issuer
    lateinit var currentInstallment : PayerCosts


    init {
        repository.onSuccessIdCall = {
            paymentMethods.value = it
        }
        repository.onSuccessIsuersCall = {
            issuers.value = it
        }
        repository.onSuccessInstallmentsCall = {
            installments.value = it
        }
    }

    fun getPaymentMethods(){
        if(paymentMethods.value.isNullOrEmpty()){
            repository.getPaymentMethods(publicKey)
        }
    }

    fun getIssuers(){
        if(issuers.value.isNullOrEmpty()){
            repository.getIssuers(publicKey,currentPaymentMethod.id)
        }
    }
    fun getInstallments(){
        if(installments.value.isNullOrEmpty()){
            repository.getInstallments(publicKey,currentAmount,currentPaymentMethod.id,currentIssuer.id)
        }
    }

    fun setAmount(amount: String) {
        currentAmount = amount
        onWizardNext?.invoke()
    }


    fun setPaymentMethod(paymentMethod: PaymentMethod) {
        currentPaymentMethod = paymentMethod
        issuers.value?.clear()
        onWizardNext?.invoke()
    }

    fun setIssuer(issuer: Issuer) {
        currentIssuer = issuer
        onWizardNext?.invoke()
        installments.value?.clear()
    }

    fun setInstallment(installment: PayerCosts) {
        currentInstallment = installment
        onWizardNext?.invoke()

    }

    fun goToFirst() {
        currentAmount = ""
        goToFirstStep?.invoke()
    }
}

