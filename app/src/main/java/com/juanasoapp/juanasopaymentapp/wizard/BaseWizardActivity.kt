package com.juanasoapp.juanasopaymentapp.wizard

import android.os.Bundle
import androidx.annotation.Nullable
import com.juanasoapp.juanasopaymentapp.R


abstract class BaseWizardActivity : BaseFragmentContainerActivity(), WizardListener {

    private lateinit var fragmentNextAnim: AnimResHelper
    private lateinit var fragmentPreviousAnim: AnimResHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentNextAnim = AnimResHelper(
                R.anim.slide_in_left, R.anim.slide_out_left,
                R.anim.slide_in_right, R.anim.slide_out_right
        )
        fragmentPreviousAnim = AnimResHelper(
                R.anim.slide_in_right, R.anim.slide_out_right,
                R.anim.slide_in_right, R.anim.slide_out_right)

    }

    private fun goToNextWizardStep(fragmentName: String) {
        showFragment(fragmentName, this.fragmentNextAnim)
    }

    private fun goToPreviousWizardStep(fragmentName: String) {
        showFragment(fragmentName, this.fragmentPreviousAnim)
    }

    override fun onPreviousWizardStep() {
        val previousFragmentName = getPreviousFragmentName(getActiveFragmentTag())
        goToPreviousWizardStep(previousFragmentName)
    }

    override fun onNextWizardStep() {
        val nextFragmentName = getNextFragmentName(getActiveFragmentTag())
        goToNextWizardStep(nextFragmentName)
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onFinishWizard() {
        finish()
    }

    protected abstract fun getWizardTotalStep(): Int

    @Nullable
    protected abstract fun getNextFragmentName(@Nullable currentFragmentName: String): String

    @Nullable
    protected abstract fun getPreviousFragmentName(@Nullable currentFragmentName: String): String
}