package com.juanasoapp.juanasopaymentapp.wizard

interface WizardListener {
    fun onNextWizardStep()

    fun onPreviousWizardStep()

    fun onFinishWizard()
}