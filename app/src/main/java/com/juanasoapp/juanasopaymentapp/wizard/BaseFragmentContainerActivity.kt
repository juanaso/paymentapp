package com.juanasoapp.juanasopaymentapp.wizard

import android.os.Bundle
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.juanasoapp.juanasopaymentapp.R


abstract class BaseFragmentContainerActivity : AppCompatActivity(){

    private lateinit var fragmentNextAnim: AnimResHelper
    var showOnStart = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        if (savedInstanceState == null) {
            if(showOnStart){
                onShowStartFragment()
            }
        }
        fragmentNextAnim = AnimResHelper(
            R.anim.slide_in_left, R.anim.slide_out_left,
            R.anim.slide_in_right, R.anim.slide_out_right
        )
    }

    fun onShowStartFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(getContainerId(),instantiateFragment(getStartFragmentName()), getStartFragmentName())
            .commit()
    }

    open fun getLayout(): Int {
        return R.layout.base_fragment_container_layout_activity
    }

    open fun getContainerId(): Int {
        return R.id.container
    }

    protected abstract fun getStartFragmentName(): String

    protected abstract fun instantiateFragment(nameFragment: String): Fragment

    protected abstract fun hasFragmentBackEnabled(fragmentName: String): Boolean

    protected fun showFragment(@Nullable fragmentName: String?, @NonNull animResHelper: AnimResHelper) {
        if (fragmentName == null || fragmentName.isEmpty()) {
            //TODO track this and throw an exception
            return
        }
        if (isFragmentInstantiated(fragmentName)) {
            getFragmentTransaction()
                .setCustomAnimations(
                    animResHelper.enterStart,
                    animResHelper.enterEnd,
                    animResHelper.popStart,
                    animResHelper.popEnd
                )
                .replace(
                    getContainerId(),
                    supportFragmentManager.findFragmentByTag(fragmentName)!!
                )
                .commit()
        } else {
            getFragmentTransaction()
                .setCustomAnimations(
                    animResHelper.enterStart,
                    animResHelper.enterEnd,
                    animResHelper.popStart,
                    animResHelper.popEnd
                )
                .replace(
                    getContainerId(),
                    instantiateFragment(fragmentName), fragmentName
                )
                .commit()
        }
    }

    protected fun getStartFragmentName(fragment: Fragment, @NonNull animResHelper: AnimResHelper) {
        getFragmentTransaction()
            .addToBackStack(null)
            .setCustomAnimations(
                animResHelper.enterStart,
                animResHelper.enterEnd,
                animResHelper.popStart,
                animResHelper.popEnd
            )
            .replace(getContainerId(), fragment)
            .commit()
    }

    protected fun getActiveFragmentTag(): String {
        return supportFragmentManager.findFragmentById(getContainerId())?.tag!!
    }

    fun isFragmentInstantiated(nameFragment: String): Boolean {
        return supportFragmentManager.findFragmentByTag(nameFragment) != null
    }

    private fun getFragmentTransaction(): FragmentTransaction {
        return supportFragmentManager.beginTransaction()
    }
}