package com.juanasoapp.juanasopaymentapp.wizard

import androidx.annotation.AnimRes
import java.io.Serializable

class AnimResHelper(
    @param:AnimRes @field:AnimRes internal val enterStart: Int, @param:AnimRes @field:AnimRes internal val enterEnd: Int, @param:AnimRes @field:AnimRes internal val popStart: Int,
    @param:AnimRes @field:AnimRes internal val popEnd: Int
) : Serializable