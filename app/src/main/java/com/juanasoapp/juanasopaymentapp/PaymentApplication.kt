package com.juanasoapp.juanasopaymentapp

import android.app.Application
import com.juanasoapp.juanasopaymentapp.network.NetworkApplication

class PaymentApplication : Application(), NetworkApplication {


    override fun getBaseUrl(): String {
       return Constants.BASE_URL
    }

    override fun goToLogin() {

    }

    override fun getTrackingIdNumber(): String {
      return ""
    }

    override fun getSessionId(): String {
        return ""
    }

    override fun getAuthorizationToken(): String {
        return ""
    }
}