package com.juanasoapp.juanasopaymentapp.network

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonWriter
import java.io.IOException
import java.util.*

internal class UnixEpochDateTypeAdapter private constructor() : TypeAdapter<Date>() {

    @Throws(IOException::class)
    override fun read(`in`: com.google.gson.stream.JsonReader): Date {
        return Date(`in`.nextLong())
    }

    @Throws(IOException::class)
    override fun write(out: JsonWriter, value: Date) {
        out.value(value.time)
    }

    companion object {
        val unixEpochDateTypeAdapter: TypeAdapter<Date> = UnixEpochDateTypeAdapter()
    }

}