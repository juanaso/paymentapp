package com.juanasoapp.juanasopaymentapp.network

import com.juanasoapp.juanasopaymentapp.network.interceptors.InfoInterceptor
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import java.lang.reflect.Type

class ServiceGenerator {

    companion object {
        private val httpLoggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        fun <S> createService(networkApplication: NetworkApplication, serviceClass: Class<S>): S {
            return RetrofitManager.getRetrofitInstance(networkApplication).create(serviceClass)
        }

        object RetrofitManager {

            private val READ_TIMEOUT = 40
            private var instance: Retrofit? = null
            private var okHttpClientInstance: OkHttpClient? = null

            fun getRetrofitInstance(context: NetworkApplication): Retrofit {
                if (instance == null) {
                    createRetrofitInstance(context)
                }
                return instance!!
            }

            private fun createRetrofitInstance(context: NetworkApplication) {
                val nullOnEmptyConverterFactory = object : Converter.Factory() {
                    fun converterFactory() = this
                    override fun responseBodyConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit) = object : Converter<ResponseBody, Any?> {
                        val nextResponseBodyConverter = retrofit.nextResponseBodyConverter<Any?>(converterFactory(), type, annotations)
                        override fun convert(value: ResponseBody) = if (value.contentLength() != 0L) nextResponseBodyConverter.convert(value) else null
                    }
                }
                val retrofitBuilder = Retrofit.Builder()
                    .baseUrl(context.getBaseUrl())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(nullOnEmptyConverterFactory)
                    .addConverterFactory(
                        GsonConverterFactory.create(
                            GsonBuilder()
                                .registerTypeAdapter(
                                    Date::class.java,
                                    UnixEpochDateTypeAdapter.unixEpochDateTypeAdapter
                                )
                                .create()
                        )
                    )
                instance = retrofitBuilder.client(getOkHttpClient(context)).build()
            }

            private fun getOkHttpClient(context: NetworkApplication): OkHttpClient {
                if (okHttpClientInstance == null) {
                    createInstanceOkHttpClient(context)
                }
                return okHttpClientInstance!!
            }

            private fun createInstanceOkHttpClient(networkApplication: NetworkApplication) {
                //TODO: check cast exception
                val okHttpClientBuilder = getBaseBuilder(networkApplication)
//                okHttpClientBuilder.addInterceptor(AuthInterceptor(networkApplication))

//                if (BuildConfig.DEBUG) {
////                    okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)
//                }
                okHttpClientInstance = okHttpClientBuilder.build()
            }

            private fun getBaseBuilder(networkApplication: NetworkApplication): OkHttpClient.Builder {
                val okHttpClientBuilder = OkHttpClient.Builder()
                    .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
                    .connectTimeout(READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
                    .writeTimeout(READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
                    .addInterceptor(InfoInterceptor(networkApplication))

//                if (BuildConfig.DEBUG) {
//                    okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)
//                }
                return okHttpClientBuilder
            }
        }
    }
}