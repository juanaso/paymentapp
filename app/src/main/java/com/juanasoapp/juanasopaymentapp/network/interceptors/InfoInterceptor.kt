package com.juanasoapp.juanasopaymentapp.network.interceptors

import com.juanasoapp.juanasopaymentapp.network.NetworkApplication
import okhttp3.Interceptor
import okhttp3.Response

class InfoInterceptor(app: NetworkApplication) : NetworkInterceptor(app) {

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(
            chain.request().newBuilder()
                .build()
        )
    }
}