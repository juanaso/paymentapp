package com.juanasoapp.juanasopaymentapp.network.interceptors

import com.juanasoapp.juanasopaymentapp.network.NetworkApplication
import okhttp3.Interceptor

abstract class NetworkInterceptor(val app: NetworkApplication) : Interceptor