package com.juanasoapp.juanasopaymentapp.network.interceptors


import com.juanasoapp.juanasopaymentapp.network.NetworkApplication
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class AuthInterceptor(app: NetworkApplication) : NetworkInterceptor(app) {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()

        return chain.proceed(builder.build())
    }
}