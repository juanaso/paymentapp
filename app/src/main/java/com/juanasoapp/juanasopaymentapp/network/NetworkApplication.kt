package com.juanasoapp.juanasopaymentapp.network

interface NetworkApplication {

    fun getBaseUrl() : String
    fun goToLogin()
    fun getTrackingIdNumber() : String
    fun getSessionId() : String
    fun getAuthorizationToken():String
}