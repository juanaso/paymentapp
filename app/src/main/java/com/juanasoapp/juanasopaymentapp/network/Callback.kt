package com.juanasoapp.juanasopaymentapp.network

import okhttp3.Headers
import retrofit2.Call
import retrofit2.Response
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import kotlin.collections.HashMap

class Callback<T>(val app: NetworkApplication) : retrofit2.Callback<T> {

    private var isSuccessful = false
    var onSuccess: ((T?) -> Unit)? = null
    var onSuccessWithHeaders: ((T?, HashMap<String, String>) -> Unit)? = null
    var onNetworkError: (() -> Unit)? = null
    var onServerError: ((String?, String?) -> Unit)? = null
    var onFinish: ((Boolean?) -> Unit)? = null
    var onUnauthorized: (() -> Unit) = { app.goToLogin() }

    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (response.isSuccessful) {
            isSuccessful = true
            onSuccess?.invoke(response.body())
            response.headers()
            onSuccessWithHeaders?.invoke(response.body(), getHeaders(response.headers()))

        } else {
            val errorResponse = getErrorResponse(response)
            when (response.code()) {
                401 -> onUnauthorized()
                500 -> {
                    onServerError?.invoke(errorResponse?.codigo, errorResponse?.descripcion)
                }
                else -> {
                    onServerError?.invoke(errorResponse?.codigo, errorResponse?.descripcion)
                }
            }
        }
        onFinish?.invoke(isSuccessful)
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        onNetworkError?.invoke()
        onFinish?.invoke(isSuccessful)
    }

    private fun getErrorResponse(response: Response<T>): ErrorResponse? {
        try {
            return GsonConverterFactory.create().responseBodyConverter(
                ErrorResponse::class.java!!,
                arrayOfNulls(0),
                null
            )?.convert(response.errorBody()!!) as ErrorResponse?
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return null
    }

    private fun getHeaders(headers: Headers): HashMap<String, String> {
        val result = HashMap<String, String>()
        for (i in 0 until headers.size()) {
            result[headers.name(i)] = headers.value(i)
        }
        return result
    }

}
